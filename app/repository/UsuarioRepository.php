<?php

namespace DWES\app\repository;

use DWES\app\entity\Usuario;
use DWES\core\App;
use DWES\core\database\QueryBuilder;
use DWES\app\entity\Grupo;

class UsuarioRepository extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('usuario', Usuario::class,  $loadDataBeforeConstruct=false);
    }

    /**
     * @return UsuarioRepository
     */
    public static function getRepository() : UsuarioRepository
    {
        return App::getRepository(UsuarioRepository::class);
    }
}
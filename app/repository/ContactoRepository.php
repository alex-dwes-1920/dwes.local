<?php

namespace DWES\app\repository;

use DWES\core\App;
use DWES\core\database\QueryBuilder;
use DWES\app\entity\Contacto;
use DWES\app\entity\Grupo;

class ContactoRepository extends QueryBuilder
{
    /**
     * @return ContactoRepository
     */
    public static function getRepository() : ContactoRepository
    {
        return App::getRepository(ContactoRepository::class);
    }

    public function __construct()
    {
        parent::__construct('contacto', Contacto::class,  $loadDataBeforeConstruct=true);
    }

    /**
     * @param Contacto $contacto
     * @return Grupo
     * @throws NotFoundException
     * @throws QueryException
     */
    public function getGrupo(Contacto $contacto) : Grupo
    {
        $grupoRepository = new GrupoRepository();
        return $grupoRepository->find($contacto->getGrupo());
    }

    /**
     * @param Contacto $contacto
     * @throws QueryException
     */
    public function nuevo(Contacto $contacto)
    {
        $fnTransaction = function () use ($contacto)
        {
            $this->save($contacto);
            $grupo = $this->getGrupo($contacto);
            $grupo->setNumContactos($grupo->getNumContactos()+1);

            $grupoRepository = new GrupoRepository();
            $grupoRepository->update($grupo);
        };

        $this->executeTransaction($fnTransaction);
    }

    /**
     * @param Contacto $contacto
     * @throws QueryException
     */
    public function elimina(Contacto $contacto)
    {
        $fnTransaction = function () use ($contacto)
        {
            $grupo = $this->getGrupo($contacto);
            $grupo->setNumContactos($grupo->getNumContactos()-1);

            $this->remove($contacto);

            App::getRepository(GrupoRepository::class)->update($grupo);
        };

        $this->executeTransaction($fnTransaction);
    }

    /**
     * @param Contacto $contacto
     * @param int $idGrupoAnterior
     * @throws QueryException
     */
    public function edita(Contacto $contacto, int $idGrupoAnterior)
    {
        $fnTransaction = function () use ($contacto, $idGrupoAnterior)
        {
            $this->update($contacto);

            if ($idGrupoAnterior !== $contacto->getGrupo())
            {
                $grupoRepository = App::getRepository(GrupoRepository::class);
                $grupoAnterior = $grupoRepository->find($idGrupoAnterior);
                $grupoAnterior->setNumContactos($grupoAnterior->getNumContactos()-1);
                $grupoRepository->update($grupoAnterior);

                $grupo = $this->getGrupo($contacto);
                $grupo->setNumContactos($grupo->getNumContactos()+1);
                $grupoRepository->update($grupo);
            }

            $this->update($contacto);
        };

        $this->executeTransaction($fnTransaction);
    }
}
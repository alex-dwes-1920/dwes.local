<?php

use Monolog\Logger;

return [
    'debug' => false,
    'database' => [
        'dbname' => 'agenda',
        'dbuser' => 'useragenda',
        'dbpassword' => 'useragenda',
        'connection' => 'mysql:host=dwes.local',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'logger' => [
        'filename' => __DIR__ . '/../logs/app.log',
        'log_level' => Logger::INFO
    ],
    'security' => [
        'roles' => [
            'ROLE_ADMIN'=>3,
            'ROLE_USER'=>2,
            'ROLE_ANONYMOUS'=>1
        ]
    ]
];
<?php

namespace DWES\app\controllers;

use DWES\app\repository\UsuarioRepository;
use DWES\core\App;
use DWES\core\exceptions\AuthenticationException;
use DWES\core\exceptions\ValidationException;
use DWES\core\helpers\FlashMessage;
use DWES\core\Response;
use DWES\core\Security;

class AuthController
{
    public function login()
    {
        $username = FlashMessage::get('username');
        $error = FlashMessage::get('error');
        Response::renderView('login', [
            'error' => $error,
            'username' => $username
        ]);
    }

    /**
     * @throws AuthenticationException
     * @throws ValidationException
     * @throws \DWES\core\exceptions\AppException
     */
    public function checkLogin()
    {
        $username = $_POST['username'] ?? '';
        $password = $_POST['password'] ?? '';

        if ($username === '' || $password === '')
        {
            $validationException = new ValidationException("El usuario y/o el password no pueden quedar vacíos");
            $validationException->setRedireccion('login');

            throw $validationException;
        }

        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'username' => $username
        ]);

        if (is_null($usuario)
            || Security::checkPassword($password, $usuario->getPassword()) === false)
        {
            $authenticationException = new AuthenticationException("El usuario y/o password no existen en la BBDD");
            $authenticationException->setRedireccion('login');
            FlashMessage::set('username', $username);
            throw $authenticationException;
        }

        $_SESSION['usuario'] = $usuario->getId();

        App::get('router')->redirect('contactos');
    }

    public function logout()
    {
        if (isset ($_SESSION['usuario']))
        {
            $_SESSION['usuario'] = null;
            unset ($_SESSION['usuario']);
        }

        App::get('router')->redirect('login');
    }
}
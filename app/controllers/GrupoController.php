<?php

namespace DWES\app\controllers;

use DWES\app\entity\Grupo;
use DWES\app\repository\GrupoRepository;
use DWES\core\App;
use DWES\core\exceptions\ValidationException;
use DWES\core\Response;

class GrupoController
{
    public function listar()
    {
        $grupos = App::getRepository(GrupoRepository::class)->findAll();

        Response::renderView(
            'grupos',
            [
                'grupos' => $grupos
            ]
        );
    }

    public function nuevo()
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');

        $nombre = $_POST['nombre'];

        $grupo = new Grupo();
        $grupo->setNombre($nombre);

        GrupoRepository::getRepository()->save($grupo);

        $mensaje = "El grupo se ha insertado correctamente";

        App::get('router')->redirect('grupos');
    }

    /**
     * @param int $id
     * @throws \DWES\core\exceptions\AppException
     * @throws \DWES\core\exceptions\NotFoundException
     * @throws \DWES\core\exceptions\QueryException
     */
    public function eliminar(int $id)
    {
        $grupoRepository = GrupoRepository::getRepository();

        $grupo = $grupoRepository->find($id);
        /** @var Grupo $grupo */
        $grupoRepository->elimina($grupo);

        App::get('router')->redirect('grupos');
    }
}
<?php

namespace DWES\app\controllers;

use DWES\app\entity\Contacto;
use DWES\app\repository\ContactoRepository;
use DWES\app\repository\GrupoRepository;
use DWES\app\utils\File;
use DWES\core\App;
use DWES\core\exceptions\ValidationException;
use DWES\core\helpers\FlashMessage;
use DWES\core\helpers\MyLogger;
use DWES\core\Response;

class ContactoController
{
    /**
     * @return ContactoRepository
     */
    private function getContactoRepository() : ContactoRepository
    {
        return App::getRepository(ContactoRepository::class);
    }

    public function show(int $id)
    {
        /** @var Contacto $contacto */
        $contacto = ContactoRepository::getRepository()->find($id);

        Response::renderView('show-contacto', [ 'contacto' => $contacto]);
    }

    private function generaImagenFoto(string $foto)
    {
        if (is_file($foto) === true)
            $imagen = imagecreatefrompng($foto);
        else
        {
            $alto = 200;
            $ancho = 200;
            $imagen = imagecreatetruecolor($ancho, $alto);
            $blanco = imagecolorallocate($imagen, 255, 255, 255);
            $azul = imagecolorallocate($imagen, 0, 0, 64);
            imagefill($imagen, 0, 0, $azul);
            imagestring($imagen, 4, 50, 150, 'SIN FOTO', $blanco);
        }

        header("Content-Type: image/png");
        imagepng($imagen);
        imagedestroy($imagen);
    }

    public function getFoto(int $id)
    {
        /** @var Contacto $contacto */
        $contacto = ContactoRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $contacto->getFoto();

        $this->generaImagenFoto($foto);
    }

    public function getMiniatura(int $id)
    {
        /** @var Contacto $contacto */
        $contacto = ContactoRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $contacto->getMiniatura();

        $this->generaImagenFoto($foto);
    }

    /**
     * @throws \DWES\core\exceptions\QueryException
     */
    public function listar()
    {
        $grupos = GrupoRepository::getRepository()->findAll();

        $contactoRepository = ContactoRepository::getRepository();
        $contactos = $contactoRepository->findBy(
            ['usuario' => App::get('usuario')->getId()]
        );

        $mensaje = FlashMessage::get('mensaje');
        $error = FlashMessage::get('error');
        $numContactos = count($contactos);

        Response::renderView('contactos', [
            'contactos' => $contactos,
            'numContactos' => $numContactos,
            'grupos' => $grupos,
            'contactoRepository' => $contactoRepository,
            'mensaje' => $mensaje,
            'error' => $error
        ]);
    }

    /**
     * @param string $messsage
     * @return ValidationException
     */
    private function getValidationException(string $messsage): ValidationException
    {
        $validationException = new ValidationException($messsage);
        $validationException->setRedireccion('contactos');
        return $validationException;
    }

    private function creaFoto(Contacto & $contacto)
    {
        $file = new File(
            'foto',
            'uploads/',
            ['image/png']
        );

        $file->uploadFile();

        $foto = $file->getFileUrl();

        $contacto->setFoto($foto);
    }
    /**
     * @throws ValidationException
     * @throws \DWES\core\exceptions\AppException
     */
    public function nuevo()
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw $this->getValidationException('El campo nombre no se puede quedar vacío');
        if(!isset($_POST['telefono']) || empty($_POST['telefono']))
            throw $this->getValidationException('El campo teléfono no se puede quedar vacío');
        if(!isset($_POST['grupo']) || empty($_POST['grupo']))
            throw $this->getValidationException('El campo grupo no se puede quedar vacío');

        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $grupo = $_POST['grupo'];

        $contacto = new Contacto();
        $contacto->setNombre($nombre);
        $contacto->setTelefono($telefono);
        $contacto->setGrupo($grupo);
        $contacto->setUsuario(App::get('usuario')->getId());

        $this->creaFoto($contacto);

        $this->getContactoRepository()->nuevo($contacto);

        $mensaje = "Se ha insertado correctamente el contacto con nombre $nombre";
        App::getService(MyLogger::class)->addMessage($mensaje);

        FlashMessage::set('mensaje', $mensaje);

        App::get('router')->redirect('contactos');
    }

    /**
     * @throws \DWES\core\exceptions\QueryException
     */
    public function editar(int $id)
    {
        $grupos = GrupoRepository::getRepository()->findAll();

        $contactoRepository = ContactoRepository::getRepository();
        $contactos = $contactoRepository->findBy(
            ['usuario' => App::get('usuario')->getId()]
        );

        Response::renderView('contactos', [
            'id' => $id,
            'contactos' => $contactos,
            'grupos' => $grupos,
            'contactoRepository' => $contactoRepository
        ]);
    }

    /**
     * @param int $id
     * @throws ValidationException
     * @throws \DWES\core\exceptions\AppException
     * @throws \DWES\core\exceptions\NotFoundException
     * @throws \DWES\core\exceptions\QueryException
     */
    public function actualizar(int $id)
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');
        if(!isset($_POST['telefono']) || empty($_POST['telefono']))
            throw new ValidationException('El campo teléfono no se puede quedar vacío');
        if(!isset($_POST['grupo']) || empty($_POST['grupo']))
            throw new ValidationException('El campo grupo no se puede quedar vacío');

        /** @var Contacto $contacto */
        $contacto = ContactoRepository::getRepository()->find($id);

        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $grupo = $_POST['grupo'];
        $idGrupoAnterior = $contacto->getGrupo();

        $contacto->setNombre($nombre);
        $contacto->setTelefono($telefono);
        $contacto->setGrupo($grupo);

        ContactoRepository::getRepository()->edita($contacto, $idGrupoAnterior);

        FlashMessage::set('mensaje', 'El contacto se ha editado correctamente');

        App::get('router')->redirect('contactos');
    }

    /**
     * @throws \DWES\core\exceptions\AppException
     * @throws \DWES\core\exceptions\NotFoundException
     * @throws \DWES\core\exceptions\QueryException
     */
    public function eliminar(int $id)
    {
        $contactoRepository = $this->getContactoRepository();

         $contacto = $contactoRepository->find($id);
        /** @var Contacto $contacto */
        $contactoRepository->elimina($contacto);

        header('Content-Type: application/json');

        echo json_encode([ 'ok' => true ]);
    }
}
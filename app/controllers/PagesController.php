<?php

namespace DWES\app\controllers;

use DWES\core\Response;

class PagesController
{
    public function inicio()
    {
        Response::renderView('inicio');
    }

    private function error(string $message, string $errorMessage, int $errorCode)
    {
        header("HTTP/1.1 $errorCode $errorMessage", true, $errorCode);
        Response::renderView('error', [
            'message' => $message,
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage
        ]);
    }

    public function errorInterno(string $message)
    {
        $this->error($message, $errorMessage = 'Internal Server Error',  $errorCode = 500);
    }

    public function accesoDenegado(string $message)
    {
        $this->error($message, $errorMessage = 'Forbidden',  $errorCode = 403);
    }

    public function notFound(string $message)
    {
        $this->error($message, $errorMessage = 'Page not Found',  $errorCode = 404);
    }
}
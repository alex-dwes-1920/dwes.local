<?php

namespace DWES\app\controllers;

use DWES\app\entity\Usuario;
use DWES\app\repository\UsuarioRepository;
use DWES\core\App;
use DWES\core\exceptions\ValidationException;
use DWES\core\Response;
use DWES\core\Security;

class UsuarioController
{
    public function listar()
    {
        $usuarios = App::getRepository(UsuarioRepository::class)->findAll();

        Response::renderView(
            'usuarios',
            [
                'usuarios' => $usuarios
            ]
        );
    }

    public function nuevo()
    {
        if (!isset($_POST['username']) || empty($_POST['username']))
            throw new ValidationException('El campo username no se puede quedar vacío');

        $username = $_POST['username'];

        if (!isset($_POST['password']) || empty($_POST['password']))
            throw new ValidationException('El campo password no se puede quedar vacío');

        $password = $_POST['password'];

        $usuario = new Usuario();
        $usuario->setUsername($username);
        $usuario->setPassword(Security::encrypt($password));
        $usuario->setRole('ROLE_USER');

        UsuarioRepository::getRepository()->save($usuario);

        $mensaje = "El usuario se ha insertado correctamente";

        App::get('router')->redirect('usuarios');
    }
}
<?php

namespace DWES\app\entity;

use DateTime;

class Contacto implements IEntity
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $telefono;
    /**
     * @var string
     */
    private $foto;
    /**
     * @var DateTime
     */
    private $fechaAlta;
    /**
     * @var int
     */
    private $grupo;
    /**
     * @var int
     */
    private $usuario;


    /**
     * Contacto constructor.
     */
    public function __construct()
    {
        if (is_null($this->fechaAlta))
            $this->fechaAlta = new DateTime();
        else
            $this->fechaAlta = DateTime::createFromFormat('Y-m-d H:i:s', $this->fechaAlta);
    }

    public function __toString()
    {
        return 'ID:  ' . $this->id .
            ' NOMBRE: ' . $this->nombre .
            ' TELÉFONO: ' . $this->telefono;
    }

    public function __clone()
    {
        $this->fechaAlta = clone $this->fechaAlta;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Contacto
     */
    public function setId(int $id): Contacto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Contacto
     */
    public function setNombre(string $nombre): Contacto
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefono(): string
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     * @return Contacto
     */
    public function setTelefono(string $telefono): Contacto
    {
        $this->telefono = $telefono;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoto(): string
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     * @return Contacto
     */
    public function setFoto(string $foto): Contacto
    {
        $this->foto = $foto;
        return $this;
    }

    public function getMiniatura()
    {
        $filePath = $this->foto;
        $posBarra = strripos($filePath, '/');
        $miniaturaPath = substr($filePath, 0, $posBarra);
        $miniaturaPath .= '/miniaturas';
        $miniaturaPath .= substr($filePath, $posBarra);
        return $miniaturaPath;
    }

    /**
     * @return DateTime
     */
    public function getFechaAlta() : DateTime
    {
        return $this->fechaAlta;
    }

    public function getFechaAltaFormateada() : string
    {
        return $this->fechaAlta->format('d/m/Y H:i:s');
    }

    /**
     * @param DateTime $fechaAlta
     * @return Contacto
     */
    public function setFechaAlta(DateTime $fechaAlta): Contacto
    {
        $this->fechaAlta = $fechaAlta;
        return $this;
    }

    /**
     * @return int
     */
    public function getGrupo(): int
    {
        return $this->grupo;
    }

    /**
     * @param int $grupo
     * @return Contacto
     */
    public function setGrupo(int $grupo): Contacto
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return int
     */
    public function getUsuario(): int
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     * @return Contacto
     */
    public function setUsuario(int $usuario): Contacto
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'telefono' => $this->telefono,
            'foto' => $this->foto,
            'grupo' => $this->grupo,
            'usuario' =>$this->usuario
        ];
    }
}
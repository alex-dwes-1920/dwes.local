<dl>
    <dt>Id:</dt>
    <dd><?= $contacto->getId() ?></dd>
    <dt>Nombre:</dt>
    <dd><?= $contacto->getNombre() ?></dd>
    <dt>Teléfono:</dt>
    <dd><?= $contacto->getTelefono() ?></dd>
    <dt>Foto:</dt>
    <dd><img src="/contactos/<?= $contacto->getId() ?>/foto" alt="<?= $contacto->getFoto() ?>"></dd>
</dl>
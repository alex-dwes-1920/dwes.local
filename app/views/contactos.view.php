<div class="row">
    <form method="post" action="/contactos/nuevo" enctype="multipart/form-data">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre">
        <label for="telefono">Teléfono:</label>
        <input type="text" name="telefono">
        <label for="grupo">Grupo:</label>
        <select name="grupo">
            <?php foreach ($grupos as $grupo) : ?>
                <option value="<?= $grupo->getId() ?>"><?= $grupo->getNombre() ?></option>
            <?php endforeach; ?>
        </select>
        <label for="foto">Foto:</label>
        <input type="file" name="foto">
        <input type="submit" value="Enviar" name="enviar">
    </form>
</div>
<div class="row">
    <?php if (isset($id)) :?>
        <form action="/contactos/<?= $id ?>/actualizar" method="post">
    <?php endif; ?>
    <table class="table">
        <caption><?php printf(ngettext("Hay %d contacto", "Hay %d contactos", $numContactos), $numContactos); ?></caption>
        <thead>
        <tr>
            <th scope="col">Operaciones</th>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Fecha alta</th>
            <th scope="col">Foto</th>
            <th scope="col">Grupo</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($contactos as $contacto ) : ?>
            <tr>
            <?php if (isset($id) && $id == $contacto->getId()) :?>
                <td>
                    <div class="btn-group" role="group" aria-label="Operaciones">
                        <button class="btn btn-secondary"><i class="fa fa-save"></i></button>
                        <a href="/contactos" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                    </div>
                </td>
                <td>
                    <input type="text" name="id-disabled" disabled value="<?= $contacto->getId() ?>">
                </td>
                <td><input type="text" name="nombre" value="<?= $contacto->getNombre() ?>"></td>
                <td><input type="text" name="telefono" value="<?= $contacto->getTelefono() ?>"></td>
                <td><?= $contacto->getFechaAltaFormateada() ?></td>
                <td><img width="80" src="<?= $contacto->getFoto() ?>" alt="<?= $contacto->getFoto() ?>"></td>
                <td>
                    <select name="grupo">
                        <?php foreach ($grupos as $grupo) : ?>
                            <option value="<?= $grupo->getId() ?>" <?= ($grupo->getId() === $contacto->getGrupo()) ? 'selected' : '' ?>><?= $grupo->getNombre() ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            <?php else: ?>
                <td>
                    <div class="btn-group" role="group" aria-label="Operaciones">
                        <a href="/contactos/<?= $contacto->getId() ?>/editar" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                        <a href="/contactos/<?= $contacto->getId() ?>" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                        <a href="/contactos/<?= $contacto->getId() ?>" class="btn btn-secondary"><i class="fa fa-eye"></i></a>
                    </div>
                </td>
                <td><?= $contacto->getId() ?></td>
                <td><?= $contacto->getNombre() ?></td>
                <td><?= $contacto->getTelefono() ?></td>
                <td><?= $contacto->getFechaAltaFormateada() ?></td>
                <td><img src="/contactos/<?= $contacto->getId() ?>/miniatura" alt="<?= $contacto->getFoto() ?>"></td>
                <td><?= $contactoRepository->getGrupo($contacto)->getNombre() ?></td>
            <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php if (isset($id)) :?>
        </form>
    <?php endif; ?>
</div>
<script src="/js/contactos.js"></script>

<?php

$router->get('', 'PagesController@inicio');
$router->get('contactos', 'ContactoController@listar', 'ROLE_USER');
$router->delete('contactos/:id', 'ContactoController@eliminar', 'ROLE_ADMIN');
$router->get('contactos/:id', 'ContactoController@show', 'ROLE_USER');
$router->post('contactos/nuevo', 'ContactoController@nuevo', 'ROLE_USER');
$router->get('contactos/:id/editar', 'ContactoController@editar', 'ROLE_USER');
$router->get('contactos/:id/foto', 'ContactoController@getFoto', 'ROLE_USER');
$router->get('contactos/:id/miniatura', 'ContactoController@getMiniatura', 'ROLE_USER');
$router->post('contactos/:id/actualizar', 'ContactoController@actualizar', 'ROLE_USER');
$router->get('grupos', 'GrupoController@listar', 'ROLE_ADMIN');
$router->post('grupos/nuevo', 'GrupoController@nuevo', 'ROLE_ADMIN');
$router->get('grupos/:id/eliminar', 'GrupoController@eliminar', 'ROLE_ADMIN');
$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');
$router->get('usuarios', 'UsuarioController@listar', 'ROLE_ADMIN');
$router->post('usuarios/nuevo', 'UsuarioController@nuevo', 'ROLE_ADMIN');

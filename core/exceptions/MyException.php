<?php

namespace DWES\core\exceptions;

use Exception;

class MyException extends Exception
{
    private $redireccion;

    /**
     * @param string $redireccion
     * @return MyException
     */
    public function setRedireccion(string $redireccion) : MyException
    {
        $this->redireccion = $redireccion;
        return $this;
    }

    /**
     * @return string
     */
    public function getRedireccion() : ?string
    {
        return $this->redireccion;
    }
}
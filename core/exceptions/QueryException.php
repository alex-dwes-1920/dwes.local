<?php

namespace DWES\core\exceptions;

use DWES\core\App;

class QueryException extends MyException
{
    public function __construct(string $appMessage, string $pdoMessage)
    {
        if (App::get('config')['debug'] === true)
            parent::__construct("$appMessage ERROR SQL: $pdoMessage");
        else
            parent::__construct($appMessage);
    }
}
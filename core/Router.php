<?php

namespace DWES\core;

use DWES\core\exceptions\AccessDeniedException;
use DWES\core\exceptions\AppException;
use DWES\core\exceptions\AuthenticationException;
use DWES\core\exceptions\NotFoundException;
use DWES\core\exceptions\QueryException;

class Router
{
    private $routes = [
        'GET' => [],
        'POST' => [],
        'DELETE' => []
    ];

    public static function load(string $file)
    {
        $router = new static;
        require $file;
        return $router;
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $role
     */
    public function get(string $uri, string $controller, string $role='ROLE_ANONYMOUS')
    {
        $this->routes['GET'][$uri] = [
            'controller' => $controller,
            'role'=>$role
        ];
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $role
     */
    public function post(string $uri, string $controller, string $role='ROLE_ANONYMOUS') {
        $this->routes['POST'][$uri] = [
            'controller' => $controller,
            'role'=>$role
        ];;
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $role
     */
    public function delete(string $uri, string $controller, string $role='ROLE_ANONYMOUS')
    {
        $this->routes['DELETE'][$uri] = [
            'controller' => $controller,
            'role'=>$role
        ];;
    }

    /**
     * @param string $controller
     * @param string $action
     * @return mixed
     * @throws AppException
     */
    private function callAction(string $controller, string $action, array $parameters)
    {
        $controller = "DWES\\app\\controllers\\$controller";

        $objController = new $controller;
        if(! method_exists($objController, $action))
        {
            throw new AppException(
                "El controlador $controller no responde al action $action");
        }

        call_user_func_array([$objController, $action], $parameters);

        return true;
    }

    /**
     * @param string $route
     * @return string
     */
    private function prepareRoute(string $route) : string
    {
        $urlRule = preg_replace(
            '/:([^\/]+)/',
            '(?<\1>[^/]+)',
            $route
        );
        return str_replace('/','\/', $urlRule);
    }

    /**
     * @param string $route
     * @param array $matches
     * @return array
     */
    private function getParametersRoute(string $route, array $matches)
    {
        preg_match_all('/:([^\/]+)/', $route, $parameterNames);

        return array_intersect_key($matches, array_flip($parameterNames[1]));
    }

    /**
     * @param string $uri
     * @param string $method
     * @return mixed
     * @throws AppException
     * @throws AuthenticationException
     */
    public function direct(string $uri, string $method)
    {
        foreach ($this->routes[$method] as $route => $operation) {
            $urlRule = $this->prepareRoute($route);
            if (preg_match('/^' . $urlRule . '\/*$/s', $uri, $matches)) {
                if (!Security::isUserGranted($operation['role'])) {
                    if (Security::isLogged())
                        throw new AccessDeniedException('No tienes permiso para realizar esta operación.');

                    $authenticationException = new AuthenticationException("Tienes que hacer login para acceder a $uri");
                    $authenticationException->setRedireccion('login');
                    throw $authenticationException;
                }

                $parameters = $this->getParametersRoute($route, $matches);
                list($controller, $action) = explode('@', $operation['controller']);

                return $this->callAction($controller, $action, $parameters);
            }
        }

        throw new NotFoundException("No se ha definido una ruta para esta URI $uri");
    }

    public function redirect(string $path)
    {
        header('location: /' . $path);
        exit;
    }

    /**
     * @param string $message
     * @param int $errorCode
     * @return mixed
     * @throws AppException
     */
    public function errorPage(string $message, int $errorCode)
    {

        switch ($errorCode)
        {
            case 500:
                $action = 'errorInterno';
                break;

            case 404:
                $action = 'notFound';
                break;

            case 403:
                $action = 'accesoDenegado';
                break;

            default:
                $action = 'errorInterno';
                $message = "Código de error no contemplado por la aplicación $errorCode";
                break;
        }
        return $this->callAction(
            'PagesController',
            $action,
            ['message' => $message]
        );
    }
}
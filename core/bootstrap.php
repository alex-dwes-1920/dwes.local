<?php

use DWES\core\App;
use DWES\core\exceptions\AppException;

try
{
    session_start();

    $config = require __DIR__ . '/../app/config.php';

    if (isset($_GET["language"]))
        $language = trim(strip_tags($_GET["language"]));
    else
    {
        if (isset($_SESSION["language"]))
            $language = $_SESSION["language"];
        else
            $language = "es_ES";
    }
    $_SESSION["language"] = $language;

    putenv("LC_ALL=$language.utf8");
    setlocale(LC_ALL, "$language.utf8");
    bindtextdomain($language, __DIR__ . "/../locale");
    bind_textdomain_codeset($language, "UTF-8");
    textdomain($language);

    App::bind('config', $config);

}catch (AppException $appException)
{
    die('Error: ' . $appException->getMessage() );
}

<?php

use DWES\app\repository\UsuarioRepository;
use DWES\core\App;
use DWES\core\exceptions\AccessDeniedException;
use DWES\core\exceptions\MyException;
use DWES\core\exceptions\NotFoundException;
use DWES\core\exceptions\QueryException;
use DWES\core\helpers\FlashMessage;
use DWES\core\Router;
use DWES\core\Request;

try
{
    require '../vendor/autoload.php';
    require '../core/bootstrap.php';

    $router = Router::load(__DIR__ . '/../app/routes.php');
    App::bind('router', $router);

    if (isset($_SESSION['usuario']))
    {
        $usuario = App::getRepository(UsuarioRepository::class)
            ->find($_SESSION['usuario']);
    }
    else
        $usuario = null;

    App::bind('usuario', $usuario);

    $router->direct(Request::uri(), Request::method());
}catch(QueryException $queryException) {
    App::get('router')->errorPage($queryException->getMessage(), 500);
}catch(AccessDeniedException $adException) {
    App::get('router')->errorPage($adException->getMessage(), 403);
}catch(NotFoundException $nfException) {
    App::get('router')->errorPage($nfException->getMessage(), 404);
}catch(MyException $myException) {
    FlashMessage::set('error', $myException->getMessage());

    $redireccion = $myException->getRedireccion();
    if (isset($redireccion) &&  $redireccion !== '')
        App::get('router')->redirect($redireccion);
}

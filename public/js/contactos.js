document.addEventListener('DOMContentLoaded', function () {
    let trashes = [...document.getElementsByClassName('fa-trash')];

    trashes.forEach(trash  => {
        trash.parentElement.addEventListener('click', function (event) {
            event.preventDefault();

            let link = event.target.parentElement;
            let url = link.href;

            fetch(url, {method: 'DELETE'}).then(response => {
                if (response.ok === true) {
                    link.closest('tr').remove();
                }
            } );
        });
    });
});